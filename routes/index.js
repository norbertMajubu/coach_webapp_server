var express = require('express');
const fs = require('fs');
var router = express.Router();

// Require our controllers.
var exercise_controller = require('../controllers/exerciseController'); 
var user_controller = require('../controllers/userController');
var ingredient_controller = require('../controllers/ingredientController');
var mealplan_controller = require('../controllers/mealplanController');
var recipe_controller = require('../controllers/recipeController');
var user_controller = require('../controllers/userController');
var preparationstep_controller = require('../controllers/preparationstepController');

var exercise_instance_controller = require('../controllers/exerciseinstanceController');
//var register_to_amazon = require('../controllers/registerToAmazon.js');



//GET request for list of all ingredients
router.get('/', function(req, res) {
      res.render('index', { title: 'MetaSweat'});
    });

/**************************************************************************************/
/*****************************INGREDIENT ROUTES****************************************/
/**************************************************************************************/
//GET request for list of all ingredients
router.get('/getingredients', ingredient_controller.ingredients_list);
// GET request for one ingredient with name.
router.get('/getingredients/:name', ingredient_controller.ingredient_return_by_name); //ingredient_detail);
router.get('/searchingredient/:name', ingredient_controller.ingredient_search_name);
// POST request for creating and Ingredient.
router.post('/ingredient/create', ingredient_controller.ingredient_create_post);
// POST request to delete and Ingredient.
//router.get('/ingredient/delete/:name', ingredient_controller.ingredient_delete_post);
//router.delete('/ingredient/delete/:name', ingredient_controller.ingredient_delete_post);
router.post('/ingredient/delete/:name', ingredient_controller.ingredient_delete_post);
// POST request to update ingredient.
router.put('/ingredient/update/:name', ingredient_controller.ingredient_update_post);
/**************************************************************************************/
/***************************PREPARATIONSTEP ROUTES*************************************/
/**************************************************************************************/
//GET request for list of all preparationsteps
router.get('/getpreparationsteps', preparationstep_controller.preparationsteps_list);
// GET request for one preparationstep with name.
router.get('/getpreparationstep/:name', preparationstep_controller.preparationstep_detail);
// GET request for one preparationstep with name.
router.get('/searchpreparationsteps/:name', preparationstep_controller.preparationstep_search);
// POST request for creating and Preparationstep.
router.post('/preparationstep/create', preparationstep_controller.preparationstep_create_post);
// POST request to delete and Preparationstep.
router.get('/ingredient/delete/:name', ingredient_controller.ingredient_delete_post);
router.post('/preparationstep/delete/:name', preparationstep_controller.preparationstep_delete_post);
router.delete('/preparationstep/delete/:name', preparationstep_controller.preparationstep_delete_post);
// POST request to update preparationstep.
router.put('/preparationstep/update/:name', preparationstep_controller.preparationstep_update_post);
/**************************************************************************************/
/************************MEALPLAN ROUTES***********************************************/
/**************************************************************************************/
//GET request for list of all mealplans
//router.get('/getmealplans', mealplan_controller.mealplans_list);
// GET request for one mealplan with name.
router.get('/getmealplan/:username', mealplan_controller.mealplan_detail);
// POST request for creating and Mealplan.
//router.post('/mealplan/create', mealplan_controller.mealplan_create_post);
// POST request to delete and Mealplan.
//router.post('/mealplan/:name/delete', mealplan_controller.mealplan_delete_post);
// POST request to update mealplan.
router.put('/mealplan/update/:username', mealplan_controller.mealplan_update_post);
/**************************************************************************************/
/************************RECIPE ROUTES*************************************************/
/**************************************************************************************/
//GET request for list of all recipes
router.get('/getrecipes', recipe_controller.recipes_list);
// GET request for one recipe with name.
router.get('/getrecipe/:name', recipe_controller.recipe_detail);
// GET request for one recipe with name.
router.get('/searchrecipes/:name', recipe_controller.recipe_search);
// POST request for creating and Recipe.
router.post('/recipe/create', recipe_controller.recipe_create_post);
// POST request to delete and Recipe.
router.post('/recipe/delete/:name', recipe_controller.recipe_delete_post);
// POST request to update recipe.
router.put('/recipe/update/:name', recipe_controller.recipe_update_post);
/**************************************************************************************/
/************************USER ROUTES*************************************************/
/**************************************************************************************/
//GET request for list of all users
router.get('/getusers', user_controller.users_list);
// GET request for one user with name.
router.get('/getuser/:username', user_controller.user_detail);
// GET request for one user with name.
router.get('/searchusers/:username', user_controller.user_search);
// POST request for creating and User.
router.post('/user/create', user_controller.user_create_post);
// POST request to delete and User.

//TODO: KENE IDE ENCRYTPION...BIZONYOS LINKEK ENCRYPTE KENE, HOGY LEGYENEK
router.post('/user/delete/:name', user_controller.user_delete_post);
// POST request to update user.
router.put('/user/update/:name', user_controller.user_update_post);
router.post('/user/update/:name', user_controller.user_update_post);
/**************************************************************************************/
/************************CLIENT ROUTES*************************************************/
/**************************************************************************************/
//GET request for list of all clients
/*router.get('/getclients', client_controller.clients_list);
// GET request for one client with name.
router.get('/getclients/:name', client_controller.client_detail);
// POST request for creating and Client.
router.post('/client/create', client_controller.client_create_post);
// POST request to delete and Client.
router.post('/client/delete/:name', client_controller.client_delete_post);
// POST request to update client.
router.post('/client/update/:name', client_controller.client_update_post);*/
/**************************************************************************************/
/**************************************************************************************/
/**************************************************************************************/



// GET request for creating a Exercise. NOTE This must come before routes that display Exercise (uses name).
router.get('/exercise/create', exercise_controller.exercise_create_get);

// POST request for creating Exercise.
router.post('/exercise/create', exercise_controller.exercise_create_post);

// GET request to delete Exercise.
router.get('/exercise/:name/delete', exercise_controller.exercise_delete_get);

// POST request to delete Exercise.
router.post('/exercise/:name/delete', exercise_controller.exercise_delete_post);

// GET request to update Exercise.
router.get('/exercise/:name/update', exercise_controller.exercise_update_get);

// POST request to update Exercise.
router.post('/exercise/:name/update', exercise_controller.exercise_update_post);

// GET request for one Exercise.
router.get('/exercise/:name', exercise_controller.exercise_detail);

// GET request for list of all Exercise.
router.get('/exercises', exercise_controller.exercise_list);


/// BOOKINSTANCE ROUTES ///

// GET request for creating a ExerciseInstance. NOTE This must come before route that displays ExerciseInstance (uses name).
router.get('/exerciseinstance/create', exercise_instance_controller.exerciseinstance_create_get);

// POST request for creating ExerciseInstance.
router.post('/exerciseinstance/create', exercise_instance_controller.exerciseinstance_create_post);

// GET request to delete ExerciseInstance.
router.get('/exerciseinstance/:name/delete', exercise_instance_controller.exerciseinstance_delete_get);

// POST request to delete ExerciseInstance.
router.post('/exerciseinstance/:name/delete', exercise_instance_controller.exerciseinstance_delete_post);

// GET request to update ExerciseInstance.
router.get('/exerciseinstance/:name/update', exercise_instance_controller.exerciseinstance_update_get);

// POST request to update ExerciseInstance.
router.post('/exerciseinstance/:name/update', exercise_instance_controller.exerciseinstance_update_post);

// GET request for one ExerciseInstance.
router.get('/exerciseinstance/:name', exercise_instance_controller.exerciseinstance_detail);

// GET request for list of all ExerciseInstance.
router.get('/exerciseinstances', exercise_instance_controller.exerciseinstance_list);



module.exports = router;
