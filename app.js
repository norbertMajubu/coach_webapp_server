var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var rsaValidation = require('auth0-api-jwt-rsa-validation');
var jwt = require('express-jwt');
var jwks = require('jwks-rsa');

var index = require('./routes/index');
var userController = require('./controllers/userController')


//Safety and efficiency:
//Check: https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/deployment
var compression = require('compression');
var helmet = require('helmet');

const cors = require('cors');

// Create the Express application object
var app = express();
app.use(cors());

app.use(helmet());

// Set up mongoose connection
var mongoose = require('mongoose');
var dev_db_url = 'mongodb://asdf:newton@ds245357.mlab.com:45357/local_library_23'
var mongoDB = process.env.MONGODB_URI || dev_db_url;
//var mongoDB = process.env.MONGODB_URI;
mongoose.connect(mongoDB, {
  useMongoClient: true
});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

//needed for cognito to function: https://github.com/aws/amazon-cognito-identity-js/issues/231
//global.navigator = () => null;



//auth0 api
var jwtCheck = jwt({
  secret: jwks.expressJwtSecret({
      cache: true,
      rateLimit: true,
      jwksRequestsPerMinute: 5,
      jwksUri: "https://metasweat.eu.auth0.com/.well-known/jwks.json"
  }),
  audience: 'metasweat_api',
  issuer: "https://metasweat.eu.auth0.com/",
  algorithms: ['RS256']
});

// Enable the use of the jwtCheck middleware in all of our routes
app.use(jwtCheck);

// If we do not get the correct credentials, we’ll return an appropriate message
app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401).json({message:'Missing or invalid token lolo'});
  }
});


// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// Uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());



app.use(express.static(path.join(__dirname, 'public')));


app.use(compression()); // Compress all routes
app.use('/', index);

// Catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// Error handler
app.use(function(err, req, res, next) {
  // Set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // Render the error page
  res.status(err.status || 500);
  res.render('error');
});


/*
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});*/
// Add headers
app.use('*',function (req, res, next) {

  // Website you wish to allow to connect
  res.header("Access-Control-Allow-Origin", "*");
  //res.setHeader('Access-Control-Allow-Origin', '*'); //http://localhost:4200');

  // Request methods you wish to allow
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.header('Access-Control-Allow-Headers',  "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.header('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

//app.use(cors());
/*
pp.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});*/
/*
var server = require('http').Server(app);
var io = require('socket.io')(server, {origins:'mydomain.com:* http://mydomain.com:* http://www.mydomain.com:*'});

server.listen([8080], ['127.0.0.1'], function(){
console.log("Server up and running...");
});
*/


userController.create_user_roles();

module.exports = app;
