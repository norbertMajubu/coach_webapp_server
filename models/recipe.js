var mongoose = require('mongoose');

var Schema = mongoose.Schema;


var RecipeSchema = new Schema({
    name: {type: String, required: true},
    meal: {type: String, enum: ['breakfast', 'snack', 'brunch', 'lunch', 'dinner']},
    energy: {type: Number, required: false},
    fat: {type: Number, required: false},
    carbs: {type: Number, required: false},
    protein: {type: Number, required: false},
    salt: {type: Number, required: false},
    preparation: [{type: Schema.ObjectId, ref: 'PreparationStep', required: false}],
    dosed_ingredients: [{
      ingredient: { type: Schema.ObjectId, ref: 'Ingredient' },
      dose: Number,
      dose_type_entity: { type: String, enum: ['pieces', 'package', 'box', 'teaspoon', 'tablespoon','cup']},
      dose_type_physical: {  type: String,  enum: ['gramm', 'kg']}
    }],
    price: {type: Number, required: false},
    prep_time: {type: Number, required: false},
    img: {type: String, required: false}
});

// Virtual for this recipe instance URL.
RecipeSchema
.virtual('url')
.get(function () {
  return '/recipe/'+this._id;
});

// Export model.
module.exports = mongoose.model('Recipe', RecipeSchema);
