var mongoose = require('mongoose');
var moment = require('moment'); // For date handling.

var Schema = mongoose.Schema;


var cognito = require('amazon-cognito-identity-js');


var poolData = { 
    UserPoolId : 'us-east-1_Z7SmWPSfz',
    ClientId : '5k9jdmqiee7d3d2rtt5hkh2siv',
};

var userPool = new cognito.CognitoUserPool(poolData);

var UserSchema = new Schema(
    {
      username: {type: String, required: true, max: 100},
      first_name: {type: String, required: true, max: 100},
      family_name: {type: String, required: true, max: 100},

      mealPlan: { type: Schema.ObjectId, ref: 'MealPlan'},      

      dynamicData: [{
        weight: Number,
        steps: Number,
        water: Number,
      }]
    }
  );


UserSchema
.virtual('is_signed_in')
.get(function () {
  var cognitoUser = userPool.getCurrentUser();
  var signedIn = (cognitoUser == null)
  return signedIn;
});

// Virtual for user "full" name.
UserSchema
.virtual('name')
.get(function () {
  return this.family_name +', '+this.first_name;
});

// Virtual for this user instance URL.
UserSchema
.virtual('url')
.get(function () {
  return '/user/'+this._id
});

UserSchema
.virtual('lifespan')
.get(function () {
  var lifetime_string='';
  if (this.date_of_birth) {
      lifetime_string=moment(this.date_of_birth).format('MMMM Do, YYYY');
      }
  lifetime_string+=' - ';
  if (this.date_of_death) {
      lifetime_string+=moment(this.date_of_death).format('MMMM Do, YYYY');
      }
  return lifetime_string
});

UserSchema
.virtual('date_of_birth_yyyy_mm_dd')
.get(function () {
  return moment(this.date_of_birth).format('YYYY-MM-DD');
});

UserSchema
.virtual('date_of_death_yyyy_mm_dd')
.get(function () {
  return moment(this.date_of_death).format('YYYY-MM-DD');
});

// Export model.
module.exports = mongoose.model('User', UserSchema);
