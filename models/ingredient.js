var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var IngredientSchema = new Schema({
    name: {type: String, required: true, min: 3, max: 100},
    energy: {type: Number, required: false},
    fat: {type: Number, required: false},
    carbs: {type: Number, required: false},
    protein: {type: Number, required: false},
    salt: {type: Number, required: false},
    price: {type: Number, required: false },
    img: {type: String, required: false}
});

// Virtual for this ingredient instance URL.
IngredientSchema
.virtual('url')
.get(function () {
  return '/ingredient/'+this._id;
});

// Export model.
module.exports = mongoose.model('Ingredient', IngredientSchema);
