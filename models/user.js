var mongoose = require('mongoose');

var Schema = mongoose.Schema;



var PermissionSchema = mongoose.Schema({
  name : {type: String, required: true},
}); // this are mostly static data

var RoleSchema = mongoose.Schema({
  name : {type: String, required: true},
  permissions : Array
});// _permissions is an array that contain permissions _id

var contacts_schema = mongoose.Schema({
  _role : Number,
  _verify_code : Number,
  _groups_id : Array,
  _service_id : String
}); // and at last _role is _id of the role which this user owns.

var UserSchema = new Schema({
    username: {type: String, required: true},
    firstname: {type: String, required: false},
    surname: {type: String, required: false},
    email: {type: String, required: false},

    mealplan: {type: Schema.ObjectId, ref: 'Mealplan', required: false},
    water: {Number,required: false},
    steps: {Number,required: false},
    trainer_id:  {type: String, required: false},

    clients: [{type: Schema.ObjectId, ref: 'Client', required: false}],

    role : {type: String, required: true},

});


// Virtual for this user instance URL.
/*UserSchema
.virtual('url')
.get(function () {
  return '/user/'+this._id;
});*/


// Export model.

var User =  mongoose.model('User', UserSchema);
var Role =mongoose.model('Role', RoleSchema);
var Permission = mongoose.model('Permission', PermissionSchema);

module.exports = {
  'User': User,
  'Role': Role,
  'Permission': Permission
}