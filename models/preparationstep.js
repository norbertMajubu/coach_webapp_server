var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var PreparationStepSchema = new Schema({
    name: {type: String, required: true, min: 3, max: 100},
    equipment_needed: [{type: String, required: false}],
    img: {type: String, required: false}
});

// Virtual for this preparationstep instance URL.
PreparationStepSchema
.virtual('url')
.get(function () {
  return '/preparationstep/'+this._id;
});

// Export model.
module.exports = mongoose.model('PreparationStep', PreparationStepSchema);
