var mongoose = require('mongoose');
var moment = require('moment');

var Schema = mongoose.Schema;

var ExerciseInstanceSchema = new Schema({
    exercise: { type: Schema.ObjectId, ref: 'Exercise', required: true }, // Reference to the associated exercise.
    imprint: {type: String, required: true},
    status: {type: String, required: true, enum:['Available', 'Maintenance', 'Loaned', 'Reserved'], default:'Maintenance'},
    due_back: { type: Date, default: Date.now },
});

// Virtual for this exerciseinstance object's URL.
ExerciseInstanceSchema
.virtual('url')
.get(function () {
  return '/exerciseinstance/'+this._id;
});


ExerciseInstanceSchema
.virtual('due_back_formatted')
.get(function () {
  return moment(this.due_back).format('MMMM Do, YYYY');
});

ExerciseInstanceSchema
.virtual('due_back_yyyy_mm_dd')
.get(function () {
  return moment(this.due_back).format('YYYY-MM-DD');
});


// Export model.
module.exports = mongoose.model('ExerciseInstance', ExerciseInstanceSchema);
