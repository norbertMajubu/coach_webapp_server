var mongoose = require('mongoose');

var Schema = mongoose.Schema;


var MealPlanSchema = new Schema({
    //nutritionProfile: { type: Schema.ObjectId, ref: 'NutrionProfile', required: false },
    username: {type: String, required: true},
    energy: {type: Number, required: false},
    fat: {type: Number, required: false},
    carbs: {type: Number, required: false},
    protein: {type: Number, required: false},
    recipes: [{
      type: Schema.ObjectId, ref: 'Recipe',
      date: Date,default: Date.now   
    }],
});

// Virtual for this recipe instance URL.
MealPlanSchema
.virtual('url')
.get(function () {
  return '/recipe/'+this._id;
});

// Export model.
module.exports = mongoose.model('MealPlan', MealPlanSchema);
