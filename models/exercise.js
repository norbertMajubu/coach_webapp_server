var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ExerciseSchema = new Schema({
    title: {type: String, required: true},
    user: { type: Schema.ObjectId, ref: 'User', required: true },
    summary: {type: String, required: true},
    isbn: {type: String, required: true},
    genre: [{ type: Schema.ObjectId, ref: 'Genre' }]
});

// Virtual for this exercise instance URL.
ExerciseSchema
.virtual('url')
.get(function () {
  return '/exercise/'+this._id;
});

// Export model.
module.exports = mongoose.model('Exercise', ExerciseSchema);
