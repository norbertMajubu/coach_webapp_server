var Preparationstep = require('../models/preparationstep');
var Recipe = require('../models/recipe');
var async = require('async');

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

// Display list of all Preparationstep.
exports.preparationsteps_list = function(req,res,next){

    Preparationstep.find()
    .sort([['name', 'ascending']])
    .exec(function (err, list_preparationsteps) {
      if (err) { return next(err); }
      // Successful, so render.
      if(req.params.name){
        //res.json(list_preparationsteps[req.params.name]);
      }
      else{
        res.json(list_preparationsteps);
      }
     // res.render('preparationstep_list', { title: 'Preparationstep List', list_preparationsteps:  list_preparationsteps});
    });
}

// Display detail page for a specific Preparationstep.
exports.preparationstep_detail = function(req, res, next) {

    async.parallel({
        /*preparationstep: function(callback) {
            Preparationstep.findById(req.params.name)
              .exec(callback);
        },*/
        preparationstep: function(callback) {
            var query = { name:  req.params.name};
            var retOptions = {_id:0, _v:0}
            Preparationstep.find(query, retOptions)
              .exec(callback);
        },

        //TODO: THIS IS HOW YOU RETURN THE FOODS RELATED WITH IT
        /*preparationstep_recipes: function(callback) {
          Recipe.find({ 'preparationstep': req.params.name })
          .exec(callback);
        },*/

    }, function(err, results) {
        if (err) { return next(err); }
        if (results.preparationstep==null) { // No results.
            res.json('')
        }
        else{
            console.log(results.preparationstep[0])
            res.json(results.preparationstep[0])
        }
    });
};

// Display detail page for a specific Preparationstep.
exports.preparationstep_search = function(req, res, next) {

    async.parallel({
        preparationstep: function(callback) {
            var query = { name:  new RegExp(".*" + req.params.name + ".*", "g")};
            var retOptions = {_id:0, _v:0}
            Preparationstep.find(query, retOptions)
              .exec(callback);
        },

        //TODO: THIS IS HOW YOU RETURN THE FOODS RELATED WITH IT
        /*preparationstep_recipes: function(callback) {
          Recipe.find({ 'preparationstep': req.params.name })
          .exec(callback);
        },*/

    }, function(err, results) {
        if (err) { return next(err); }
        if (results.preparationstep==null) { // No results.
            res.json('')
        }
        else{
            res.json(results.preparationstep)
        }
    });
};

// Handle Preparationstep create on POST.
exports.preparationstep_create_post = [

    // Validate that the name field is not empty.
    body('name', 'Preparationstep name required').isLength({ min: 1 }).trim(),

    // Sanitize (trim and escape) the name field.
    sanitizeBody('name').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);
        // Create a preparationstep object with escaped and trimmed data.
        var preparationstep = new Preparationstep(
          { 
            name:req.body.name,
            energy: req.body.energy,
            fat: req.body.fat,
            carbs: req.body.carbs,
            protein: req.body.protein,
            salt: req.body.salt,
            price:req.body.price,
            img: req.body.img,
           }
        );


        if (!errors.isEmpty()) {
            // There are errors. Render the form again with sanitized values/error messages.
            res.render('preparationstep_form', { title: 'Create Preparationstep', preparationstep: preparationstep, errors: errors.array()});
        return;
        }
        else {
            // Data from form is valid.
            // Check if Preparationstep with same name already exists.
            Preparationstep.findOne({ 'name': req.body.name })
                .exec( function(err, found_preparationstep) {
                     if (err) { return next(err); }

                     if (found_preparationstep) {
                         // Preparationstep exists, redirect to its detail page.
                         res.redirect(found_preparationstep.url);
                     }
                     else {

                         preparationstep.save(function (err) {
                           if (err) { return next(err); }
                           // Preparationstep saved. Redirect to preparationstep detail page.
                           res.json(preparationstep);
                         });

                     }

                 });
        }
    }
];

// Handle Preparationstep delete on POST.
exports.preparationstep_delete_post = function(req, res, next) {

    async.parallel({
        preparationstep: function(callback) {
            Preparationstep.findById(req.params.name).exec(callback);
        },
        preparationstep_recipes: function(callback) {
            //TODO: PREPARATION IS AN ARRAY SO IT HAS TO BE HANDLED IN A MORE SOPHISTICATED WAY
            Recipe.find({ 'preparation': req.params.name }).exec(callback);
        },
    }, function(err, results) {
        if (err) { return next(err); }
        // Success
       /* if (results.preparationstep_recipes.length 0) {
            // Preparationstep belongs to recipes. Render in same way as for GET route.
            res.end('Prep step belongs to existing recipes') //'preparationstep_delete', { title: 'Delete Preparationstep', preparationstep: results.preparationstep, preparationstep_recipes: results.preparationstep_recipes } );
            return;
        }
        else {*/
            // Preparationstep has no recipes. Delete object and redirect to the list of preparationsteps.
            Preparationstep.findByIdAndRemove(req.body.name, function deletePreparationstep(err, result) {
                if (err) { 
                    res.json(err); 
                }
                else{
                    res.json(result); 
                }
            });

       // }
    });

};
// Handle preparationstep update on POST.
exports.preparationstep_update_post = [
   
    // Validate that the name field is not empty.
    body('preparationstepname', 'preparationstep name required').isLength({ min: 1 }).trim(),
    // Sanitize (trim and escape) the name field.
    sanitizeBody('preparationstepname').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {
        // Extract the validation errors from a request .
        const errors = validationResult(req);

    // Create a preparationstep object with escaped and trimmed data (and the old name!)
        var updates = {};
        if(req.body.name != null){updates['name'] = req.body.name;};
        if(req.body.equipment_needed!= null){updates['equipment_needed'] = req.body.equipment_needed;};
        if(req.body.img != null){updates['img'] = req.body.img;};

        if (!errors.isEmpty()) {
           res.json(errors) 
        }
        else {
            Preparationstep.findByIdAndUpdate(req.params.name, updates, {}, function (err,thepreparationstep) {
                if (err) {return next(err); }
                else{
                    res.json(thepreparationstep)
                }
                });
        }
    }
];