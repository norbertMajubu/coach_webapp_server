var Exercise = require('../models/exercise');
var User = require('../models/user');
var ExerciseInstance = require('../models/exerciseinstance');

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var async = require('async');

// Display list of all exercises.
exports.exercise_list = function(req, res, next) {

  Exercise.find({}, 'title user ')
    .populate('user')
    .exec(function (err, list_exercises) {
      if (err) { return next(err); }
      // Successful, so render
      res.render('exercise_list', { title: 'Exercise List', exercise_list:  list_exercises});
    });

};

// Display detail page for a specific exercise.
exports.exercise_detail = function(req, res, next) {

    async.parallel({
        exercise: function(callback) {

            Exercise.findById(req.params.id)
              .populate('user')
//
              .exec(callback);
        },
        exercise_instance: function(callback) {

          ExerciseInstance.find({ 'exercise': req.params.id })
          .exec(callback);
        },
    }, function(err, results) {
        if (err) { return next(err); }
        if (results.exercise==null) { // No results.
            var err = new Error('Exercise not found');
            err.status = 404;
            return next(err);
        }
        // Successful, so render.
        res.render('exercise_detail', { title: 'Title', exercise:  results.exercise, exercise_instances: results.exercise_instance } );
    });

};

// Display exercise create form on GET.
exports.exercise_create_get = function(req, res, next) {

    // Get all users and users, which we can use for adding to our exercise.
    async.parallel({
        users: function(callback) {
            User.find(callback);
        },
        users: function(callback) {
            User.find(callback);
        },
    }, function(err, results) {
        if (err) { return next(err); }
        res.render('exercise_form', { title: 'Create Exercise',users:results.users, users:results.users });
    });

};

// Handle exercise create on POST.
exports.exercise_create_post = [
    // Convert the user to an array.
    (req, res, next) => {
        if(!(req.body.user instanceof Array)){
            if(typeof req.body.user==='undefined')
            req.body.user=[];
            else
            req.body.user=new Array(req.body.user);
        }
        next();
    },

    // Validate fields.
    body('title', 'Title must not be empty.').isLength({ min: 1 }).trim(),
    body('user', 'User must not be empty.').isLength({ min: 1 }).trim(),
    body('summary', 'Summary must not be empty.').isLength({ min: 1 }).trim(),
    body('isbn', 'ISBN must not be empty').isLength({ min: 1 }).trim(),
  
    // Sanitize fields.
    sanitizeBody('*').trim().escape(),
    sanitizeBody('user.*').trim().escape(),
    // Process request after validation and sanitization.
    (req, res, next) => {
        

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create a Exercise object with escaped and trimmed data.
        var exercise = new Exercise(
          { title: req.body.title,
            user: req.body.user,
            summary: req.body.summary,
            isbn: req.body.isbn,
            user: req.body.user
           });

        if (!errors.isEmpty()) {
            // There are errors. Render form again with sanitized values/error messages.

            // Get all users and users for form.
            async.parallel({
                users: function(callback) {
                    User.find(callback);
                },
                users: function(callback) {
                    User.find(callback);
                },
            }, function(err, results) {
                if (err) { return next(err); }

                // Mark our selected users as checked.
                for (let i = 0; i < results.users.length; i++) {
                    if (exercise.user.indexOf(results.users[i]._id) > -1) {
                        results.users[i].checked='true';
                    }
                }
                res.render('exercise_form', { title: 'Create Exercise',users:results.users, users:results.users, exercise: exercise, errors: errors.array() });
            });
            return;
        }
        else {
            // Data from form is valid. Save exercise.
            exercise.save(function (err) {
                if (err) { return next(err); }
                   // Successful - redirect to new exercise record.
                   res.redirect(exercise.url);
                });
        }
    }
];



// Display exercise delete form on GET.
exports.exercise_delete_get = function(req, res, next) {

    async.parallel({
        exercise: function(callback) {
            Exercise.findById(req.params.id).populate('user').populate('user').exec(callback);
        },
        exercise_exerciseinstances: function(callback) {
            ExerciseInstance.find({ 'exercise': req.params.id }).exec(callback);
        },
    }, function(err, results) {
        if (err) { return next(err); }
        if (results.exercise==null) { // No results.
            res.redirect('/exercises');
        }
        // Successful, so render.
        res.render('exercise_delete', { title: 'Delete Exercise', exercise: results.exercise, exercise_instances: results.exercise_exerciseinstances } );
    });

};

// Handle exercise delete on POST.
exports.exercise_delete_post = function(req, res, next) {

    // Assume the post has valid id (ie no validation/sanitization).

    async.parallel({
        exercise: function(callback) {
            Exercise.findById(req.params.id).populate('user').populate('user').exec(callback);
        },
        exercise_exerciseinstances: function(callback) {
            ExerciseInstance.find({ 'exercise': req.params.id }).exec(callback);
        },
    }, function(err, results) {
        if (err) { return next(err); }
        // Success
        if (results.exercise_exerciseinstances.length > 0) {
            // Exercise has exercise_instances. Render in same way as for GET route.
            res.render('exercise_delete', { title: 'Delete Exercise', exercise: results.exercise, exercise_instances: results.exercise_exerciseinstances } );
            return;
        }
        else {
            // Exercise has no ExerciseInstance objects. Delete object and redirect to the list of exercises.
            Exercise.findByIdAndRemove(req.body.id, function deleteExercise(err) {
                if (err) { return next(err); }
                // Success - got to exercises list.
                res.redirect('/exercises');
            });

        }
    });

};

// Display exercise update form on GET.
exports.exercise_update_get = function(req, res, next) {

    // Get exercise, users and users for form.
    async.parallel({
        exercise: function(callback) {
            Exercise.findById(req.params.id).populate('user').populate('user').exec(callback);
        },
        users: function(callback) {
            User.find(callback);
        },
        users: function(callback) {
            User.find(callback);
        },
        }, function(err, results) {
            if (err) { return next(err); }
            if (results.exercise==null) { // No results.
                var err = new Error('Exercise not found');
                err.status = 404;
                return next(err);
            }
            // Success.
            // Mark our selected users as checked.
            for (var all_g_iter = 0; all_g_iter < results.users.length; all_g_iter++) {
                for (var exercise_g_iter = 0; exercise_g_iter < results.exercise.user.length; exercise_g_iter++) {
                    if (results.users[all_g_iter]._id.toString()==results.exercise.user[exercise_g_iter]._id.toString()) {
                        results.users[all_g_iter].checked='true';
                    }
                }
            }
            res.render('exercise_form', { title: 'Update Exercise', users:results.users, users:results.users, exercise: results.exercise });
        });

};


// Handle exercise update on POST.
exports.exercise_update_post = [

    // Convert the user to an array.
    (req, res, next) => {
        if(!(req.body.user instanceof Array)){
            if(typeof req.body.user==='undefined')
            req.body.user=[];
            else
            req.body.user=new Array(req.body.user);
        }
        next();
    },
   
    // Validate fields.
    body('title', 'Title must not be empty.').isLength({ min: 1 }).trim(),
    body('user', 'User must not be empty.').isLength({ min: 1 }).trim(),
    body('summary', 'Summary must not be empty.').isLength({ min: 1 }).trim(),
    body('isbn', 'ISBN must not be empty').isLength({ min: 1 }).trim(),

    // Sanitize fields.
    sanitizeBody('title').trim().escape(),
    sanitizeBody('user').trim().escape(),
    sanitizeBody('summary').trim().escape(),
    sanitizeBody('isbn').trim().escape(),
    sanitizeBody('user.*').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create a Exercise object with escaped/trimmed data and old id.
        var exercise = new Exercise(
          { title: req.body.title,
            user: req.body.user,
            summary: req.body.summary,
            isbn: req.body.isbn,
            user: (typeof req.body.user==='undefined') ? [] : req.body.user,
            _id:req.params.id // This is required, or a new ID will be assigned!
           });

        if (!errors.isEmpty()) {
            // There are errors. Render form again with sanitized values/error messages.

            // Get all users and users for form
            async.parallel({
                users: function(callback) {
                    User.find(callback);
                },
                users: function(callback) {
                    User.find(callback);
                },
            }, function(err, results) {
                if (err) { return next(err); }

                // Mark our selected users as checked.
                for (let i = 0; i < results.users.length; i++) {
                    if (exercise.user.indexOf(results.users[i]._id) > -1) {
                        results.users[i].checked='true';
                    }
                }
                res.render('exercise_form', { title: 'Update Exercise',users:results.users, users:results.users, exercise: exercise, errors: errors.array() });
            });
            return;
        }
        else {
            // Data from form is valid. Update the record.
            Exercise.findByIdAndUpdate(req.params.id, exercise, {}, function (err,theexercise) {
                if (err) { return next(err); }
                   // Successful - redirect to exercise detail page.
                   res.redirect(theexercise.url);
                });
        }
    }
];

