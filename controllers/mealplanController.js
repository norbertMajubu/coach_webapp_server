var MealPlan = require('../models/mealplan');
var Recipe = require('../models/recipe');
var async = require('async');

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

// Display list of all MealPlan.
/*exports.mealplans_list = function(req,res,next){

    MealPlan.find()
    .sort([['name', 'ascending']])
    .exec(function (err, list_mealplans) {
      if (err) { return next(err); }
      // Successful, so render.
      if(req.params.name){
        //res.json(list_mealplans[req.params.name]);
      }
      else{
        res.json(list_mealplans);
      }
     // res.render('mealplan_list', { title: 'MealPlan List', list_mealplans:  list_mealplans});
    });
}
*/
// Display detail page for a specific MealPlan.
exports.mealplan_detail = function(req, res, next) {

    async.parallel({
        mealplan: function(callback) {
            var query = { username:  req.params.username};
            var retOptions = {_id:0, _v:0}
            MealPlan.find(query, retOptions)
              .exec(callback);
        },
        //TODO: THIS IS HOW YOU RETURN THE FOODS RELATED WITH IT
        /*mealplan_exercises: function(callback) {
          Recipe.find({ 'mealplan': req.params.name })
          .exec(callback);
        },*/

    }, function(err, results) {
        if (err) {console.log("error while looking for mealplan for: "+req.params.username); return next(err) }
        if (results.mealplan==null) { // No results.
            console.log("did not found mealplan for: "+req.params.username)
            res.json('')
        }
        else{
            console.log("found mealplan for: "+req.params.username + "\n" + results.mealplan[0])
            res.json(results.mealplan[0])
        }
        
    });

};

// Handle MealPlan create on POST.
exports.mealplan_create_post = [

    // Validate that the name field is not empty.
    body('name', 'MealPlan name required').isLength({ min: 1 }).trim(),

    // Sanitize (trim and escape) the name field.
    sanitizeBody('name').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);
  
        // Create a mealplan object with escaped and trimmed data.
        var mealplan = new MealPlan(
          { 
            name:req.body.name,
            energy: req.body.energy,
            fat: req.body.fat,
            carbs: req.body.carbs,
            protein: req.body.protein,
            salt: req.body.salt,
            price:req.body.price,
            img: req.body.img,
           }
        );


        if (!errors.isEmpty()) {
            // There are errors. Render the form again with sanitized values/error messages.
            res.render('mealplan_form', { title: 'Create MealPlan', mealplan: mealplan, errors: errors.array()});
        return;
        }
        else {
            // Data from form is valid.
            // Check if MealPlan with same name already exists.
            MealPlan.findOne({ 'name': req.body.name })
                .exec( function(err, found_mealplan) {
                     if (err) { return next(err); }

                     if (found_mealplan) {
                         // MealPlan exists, redirect to its detail page.
                         res.redirect(found_mealplan.url);
                     }
                     else {

                         mealplan.save(function (err) {
                           if (err) { return next(err); }
                           // MealPlan saved. Redirect to mealplan detail page.
                           res.json(mealplan);
                         });

                     }

                 });
        }
    }
];


/*
// Handle MealPlan delete on POST.
exports.mealplan_delete_post = function(req, res, next) {

    async.parallel({
        mealplan: function(callback) {
            MealPlan.findById(req.params.name).exec(callback);
        },
        mealplan_exercises: function(callback) {
            Recipe.find({ 'mealplan': req.params.name }).exec(callback);
        },
    }, function(err, results) {
        if (err) { return next(err); }
        // Success
        if (results.mealplan_exercises.length > 0) {
            // MealPlan has exercises. Render in same way as for GET route.
            res.render('mealplan_delete', { title: 'Delete MealPlan', mealplan: results.mealplan, mealplan_exercises: results.mealplan_exercises } );
            return;
        }
        else {
            // MealPlan has no exercises. Delete object and redirect to the list of mealplans.
            MealPlan.findByIdAndRemove(req.body.name, function deleteMealPlan(err) {
                if (err) { return next(err); }
                // Success - go to mealplans list.
                res.redirect('/mealplans');
            });

        }
    });
};*/

// Handle mealplan update on POST.
exports.mealplan_update_post = [
   
    // Validate that the name field is not empty.
    body('mealplanname', 'mealplan name required').isLength({ min: 1 }).trim(),
    // Sanitize (trim and escape) the name field.
    sanitizeBody('mealplanname').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {
      
        // Extract the validation errors from a request .
        const errors = validationResult(req);

    // Create a mealplan object with escaped and trimmed data (and the old name!)
        var updates = {};
        if(req.body.energy!= null){updates['energy'] = req.body.energy;};
        if(req.body.fat  != null){updates['fat'] = req.body.fat;};
        if(req.body.carbs != null){updates['carbs'] = req.body.carbs;};
        if(req.body.protein!= null){updates['protein'] = req.body.protein;};
        if(req.body.recipes  != null){updates['recipes'] = req.body.recipes;};

        if (!errors.isEmpty()) {
           res.json(errors) 
        }
        else {
            Mealplan.findByIdAndUpdate(req.params.name, updates, {}, function (err,themealplan) {
                if (err) {return next(err); }
                else{
                    res.json(themealplan)
                }
                });
        }
    }
];