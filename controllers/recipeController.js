var Recipe = require('../models/recipe');
var Recipe = require('../models/recipe');
var async = require('async');

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

// Display list of all Recipe.
exports.recipes_list = function(req,res,next){

    Recipe.find()
    .sort([['name', 'ascending']])
    .exec(function (err, list_recipes) {
      if (err) { return next(err); }
      // Successful, so render.
      if(req.params.name){
        //res.json(list_recipes[req.params.name]);
      }
      else{
        res.json(list_recipes);
      }
     // res.render('recipe_list', { title: 'Recipe List', list_recipes:  list_recipes});
    });
}

// Display detail page for a specific Recipe.
exports.recipe_detail = function(req, res, next) {

    async.parallel({
        recipe: function(callback) {
            var query = { name:  req.params.name};
            var retOptions = {_id:0, _v:0}
            Recipe.find(query, retOptions)
              .exec(callback);
        },
        //TODO: THIS IS HOW YOU RETURN THE FOODS RELATED WITH IT
        /*recipe_mealplans: function(callback) {
          Recipe.find({ 'recipe': req.params.name })
          .exec(callback);
        },*/
    }, function(err, results) {
        if (err) { return next(err); }
        if (results.recipe==null) { // No results.
            res.json('')
        }
        else{
            console.log(results.recipe[0])
            res.json(results.recipe[0])
        }   
    });
};

// Display detail page for a specific Recipe.
exports.recipe_search = function(req, res, next) {

    async.parallel({
        recipe: function(callback) {
            var query = { name:  new RegExp(".*" + req.params.name + ".*", "g")};
            var retOptions = {_id:0, _v:0}
            Recipe.find(query, retOptions)
              .exec(callback);
        },
        //TODO: THIS IS HOW YOU RETURN THE FOODS RELATED WITH IT
        /*recipe_mealplans: function(callback) {
          Recipe.find({ 'recipe': req.params.name })
          .exec(callback);
        },*/
    }, function(err, results) {
        if (err) { return next(err); }
        if (results.recipe==null) { // No results.
            res.json('')
        }
        else{
            res.json(results.recipe)
        }   
    });
};


// Handle Recipe create on POST.
exports.recipe_create_post = [

    // Validate that the name field is not empty.
    body('name', 'Recipe name required').isLength({ min: 1 }).trim(),

    // Sanitize (trim and escape) the name field.
    sanitizeBody('name').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);
        // Create a recipe object with escaped and trimmed data.
        var recipe = new Recipe(
          { 
            name:req.body.name,
            energy: req.body.energy,
            fat: req.body.fat,
            carbs: req.body.carbs,
            protein: req.body.protein,
            salt: req.body.salt,
            price:req.body.price,
            img: req.body.img,
           }
        );


        if (!errors.isEmpty()) {
            // There are errors. Render the form again with sanitized values/error messages.
            res.render('recipe_form', { title: 'Create Recipe', recipe: recipe, errors: errors.array()});
        return;
        }
        else {
            // Data from form is valid.
            // Check if Recipe with same name already exists.
            Recipe.findOne({ 'name': req.body.name })
                .exec( function(err, found_recipe) {
                     if (err) { return next(err); }

                     if (found_recipe) {
                         // Recipe exists, redirect to its detail page.
                         res.redirect(found_recipe.url);
                     }
                     else {

                         recipe.save(function (err) {
                           if (err) { return next(err); }
                           // Recipe saved. Redirect to recipe detail page.
                           res.json(recipe);
                         });

                     }

                 });
        }
    }
];

// Handle Recipe delete on POST.
exports.recipe_delete_post = function(req, res, next) {

    async.parallel({
        recipe: function(callback) {
            Recipe.findById(req.params.name).exec(callback);
        },
        recipe_mealplans: function(callback) {
            Recipe.find({ 'recipe': req.params.name }).exec(callback);
        },
    }, function(err, results) {
        if (err) { return next(err); }
        // Success
        if (results.recipe_mealplans.length > 0) {
            // Recipe has mealplans. Render in same way as for GET route.
            res.end('Recipe belongs to existing mealplan') //res.render('recipe_delete', { title: 'Delete Recipe', recipe: results.recipe, recipe_mealplans: results.recipe_mealplans } );
        }
        else {
            // Recipe has no mealplans. Delete object and redirect to the list of recipes.
            Recipe.findByIdAndRemove(req.body.name, function deleteRecipe(err) {
                if (err) { return next(err); }
                // Success - go to recipes list.
                res.end('success')
            });

        }
    });

};

// Handle recipe update on POST.
exports.recipe_update_post = [
   
    // Validate that the name field is not empty.
    body('recipename', 'recipe name required').isLength({ min: 1 }).trim(),
    // Sanitize (trim and escape) the name field.
    sanitizeBody('recipename').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {
        // Extract the validation errors from a request .
        const errors = validationResult(req);

    // Create a recipe object with escaped and trimmed data (and the old name!)
        var updates = {};
        if(req.body.name != null){updates['name'] = req.body.name;};

        if(req.body.style!= null){updates['style'] = req.body.style;};
        if(req.body.preparation!= null){updates['preparation'] = req.body.preparation;};
        if(req.body.dosed_ingredients!= null){updates['dosed_ingredients'] = req.body.dosed_ingredients;};

        if(req.body.energy!= null){updates['energy'] = req.body.energy;};
        if(req.body.fat  != null){updates['fat'] = req.body.fat;};
        if(req.body.carbs != null){updates['carbs'] = req.body.carbs;};
        if(req.body.protein!= null){updates['protein'] = req.body.protein;};
        if(req.body.salt  != null){updates['salt'] = req.body.salt;};
        if(req.body.img != null){updates['img'] = req.body.img;};

        if (!errors.isEmpty()) {
           res.json(errors) 
        }
        else {
            Recipe.findByIdAndUpdate(req.params.name, updates, {}, function (err,therecipe) {
                if (err) {return next(err); }
                else{
                    res.json(therecipe)
                }
                });
        }
    }
];