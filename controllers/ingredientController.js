var Ingredient = require('../models/ingredient');
var Recipe = require('../models/recipe');
var async = require('async');
var jwt = require('express-jwt');

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

// Display list of all Ingredient.
exports.ingredients_list = function(req,res,next){
    Ingredient.find()
    .sort([['name', 'ascending']])
    .exec(function (err, list_ingredients) {
      if (err) { return next(err); }
      // Successful
        res.json(list_ingredients);
        
     // res.render('ingredient_list', { title: 'Ingredient List', list_ingredients:  list_ingredients});
    });
}


// Display detail page for a specific Ingredient.
exports.ingredient_detail = function(req, res, next) {
    console.log("idüsba lep be, name: " + req.params.name)
    async.parallel({
        ingredient: function(callback) {
            var query = { name:  req.params.name};
            var retOptions = {_id:0, _v:0}
            Ingredient.find(query, retOptions)
              .exec(callback);
        },

        //TODO: THIS IS HOW YOU RETURN THE FOODS RELATED WITH IT
        /*ingredient_recipes: function(callback) {
          Recipe.find({ 'ingredient': req.params.name })
          .exec(callback);
        },*/

    }, function(err, results) {
        if (err) { return next(err); }
        if (results.ingredient==null) { // No results.
            console.log("error in serach result by id: " + results.ingredient)
            res.json('')
        }
        else{
            console.log("serach result by id: " + results.ingredient)
            res.json(results.ingredient)
        }
    });

};


/*
// Display detail page for a specific Ingredient.
exports.ingredient_detail_name = function(req, res, next) {
    console.log("idüsba lep be, name ")
    async.parallel({
        ingredient: function(callback) {
            var query = { name:  req.params.name};
            var retOptions = {_id:0, _v:0}
            Ingredient.find(query, retOptions)
              .exec(callback);
        },

        //TODO: THIS IS HOW YOU RETURN THE FOODS RELATED WITH IT
        /*ingredient_recipes: function(callback) {
          Recipe.find({ 'ingredient': req.params.name })
          .exec(callback);
        },

    }, function(err, results) {
        if (err) { return next(err); }
        if (results.ingredient==null) { // No results.
            console.log("error in serach result by name: " + results.ingredient)
            res.json('') //err)
        }
        else{
            console.log("serach result by name: " + results.ingredient)
            res.json(results.ingredient)
        }
    });

};

*/


// Display detail page for a specific Ingredient.
exports.ingredient_search_name = function(req, res, next) {
    console.log("beleeep, name " +  req.params.name + ", "+ req.params.name)
    async.parallel({
        ingredient: function(callback) {
            
            var query = { name:  new RegExp(".*" + req.params.name + ".*", "g")};
            var retOptions = {_id:0, __v:0}
            console.log("terzesny "+query.name)
            Ingredient.find(
                query,
                retOptions
            ).exec(callback) 
        },

        //TODO: THIS IS HOW YOU RETURN THE FOODS RELATED WITH IT
        /*ingredient_recipes: function(callback) {
          Recipe.find({ 'ingredient': req.params.name })
          .exec(callback);
        },*/

    }, function(err, results) {
        console.log("legalabb ide eljut")
        if (err) { return next(err); }
        if (results.ingredient==null) { // No results.
            console.log("error in serach result by name: " + results.ingredient)
            res.json('')
        }
        else{
            for (var key in results.ingredient) {
                if (results.ingredient.hasOwnProperty(key)) {
                    console.log(key + " -> " + results.ingredient[key]);
                }
            }
            res.json(results.ingredient);
        }
    });

};

// Display detail page for a specific Ingredient.
exports.ingredient_return_by_name = function(req, res, next) {
    console.log("beleeep, name " +  req.params.name + ", "+ req.params.name)
    async.parallel({
        ingredient: function(callback) {
            
            //var query = { name: /.*${req.params.name}.*/ };  //${req.body.name}
            var query = { name: req.params.name};
            var retOptions = {_id:0, __v:0}
            console.log("terzesny "+query.name)
            Ingredient.find(
                query,
                retOptions
            ).exec(callback) 
        },

        //TODO: THIS IS HOW YOU RETURN THE FOODS RELATED WITH IT
        /*ingredient_recipes: function(callback) {
          Recipe.find({ 'ingredient': req.params.name })
          .exec(callback);
        },*/

    }, function(err, results) {
        console.log("legalabb ide eljut")
        if (err) { return next(err); }
        if (results.ingredient==null) { // No results.
            console.log("error in serach result by name: " + results.ingredient)
            res.json('')
        }
        else{
            console.log("serach result by name: " + results.ingredient)
            console.log("type of this shit: ", typeof(results.ingredient), "length: ", results.ingredient.length)

            for (var key in results.ingredient) {
                if (results.ingredient.hasOwnProperty(key)) {
                    console.log(key + " -> " + results.ingredient[key]);
                }
            }
            res.json(results.ingredient[0])
        }
    });

};


// Handle Ingredient create on POST.
exports.ingredient_create_post = [

    // Validate that the name field is not empty.
    body('name', 'Ingredient name required').isLength({ min: 1 }).trim(),

    // Sanitize (trim and escape) the name field.
    sanitizeBody('name').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);
        console.log("energy: " + req.body.energy)
        // Create a ingredient object with escaped and trimmed data.
        var ingredient = new Ingredient(
          { 
            name:req.body.name,
            energy: req.body.energy,
            fat: req.body.fat,
            carbs: req.body.carbs,
            protein: req.body.protein,
            salt: req.body.salt,
            price:req.body.price,
            img: req.body.img,
           }
        );


        if (!errors.isEmpty()) {
            // There are errors. Render the form again with sanitized values/error messages.
            res.render('ingredient_form', { title: 'Create Ingredient', ingredient: ingredient, errors: errors.array()});
        return;
        }
        else {
            // Data from form is valid.
            // Check if Ingredient with same name already exists.
            Ingredient.findOne({ 'name': req.body.name })
                .exec( function(err, found_ingredient) {
                     if (err) { return next(err); }

                     if (found_ingredient) {
                         // Ingredient exists, redirect to its detail page.
                         res.json(found_ingredient);
                     }
                     else {

                         ingredient.save(function (err) {
                           if (err) { return next(err); }
                           // Ingredient saved. Redirect to ingredient detail page.
                           res.json(ingredient);
                         });
                     }
                 });
        }
    }
];

// Handle Ingredient delete on POST.
exports.ingredient_delete_post = function(req, res, next) {

    async.parallel({
        ingredient: function(callback) {
            var query = { name:  req.params.name};
            var retOptions = {_id:0, _v:0}
            Ingredient.find(query, retOptions).exec(callback);
            //Ingredient.findById(req.params.name).exec(callback);
        },
        //TODO: SOPHISTICATED SEARCH METHOD HERE!!!
        ingredient_recipes: function(callback) {
            Recipe.find({ 'ingredient': req.params.name }).exec(callback);
        },
    }, function(err, results) {
        if (err) { return next(err); }
        // Success
        if (results.ingredient_recipes.length < 0) {//>
            // Ingredient has recipes. Render in same way as for GET route.
            //res.send('Ingredient belongs to existing recipe') //res.render('ingredient_delete', { title: 'Delete Ingredient', ingredient: results.ingredient, ingredient_recipes: results.ingredient_recipes } );
            //return;
        }
        else {
            // Ingredient has no recipes. Delete object and redirect to the list of ingredients.
            var query = { name:  req.params.name};
            var retOptions = {_id:0, _v:0}
            Ingredient.find(query, retOptions).remove().exec( function (err,theingredient) {
                if (err) {
                    return next(err); 
                }
                else{
                    res.json(theingredient);
                }
                });
                /*
            Ingredient.findByIdAndRemove(req.body.name, function deleteIngredient(err,result) {
                if (err) { 
                    res.json(err); 
                }
                else{
                    res.json(result); 
                }
            });
                */
        }
    });

};

// Handle ingredient update on POST.
exports.ingredient_update_post = [
   
    // Validate that the name field is not empty.
    body('name', 'ingredient name required').isLength({ min: 1 }).trim(),
    // Sanitize (trim and escape) the name field.
    sanitizeBody('name').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {
        // Extract the validation errors from a request .
        const errors = validationResult(req);
    // Create a ingredient object with escaped and trimmed data (and the old name!)
        var updates = {};
        if(req.body.name != null){updates['name'] = req.body.name;};
        if(req.body.energy!= null){updates['energy'] = req.body.energy;};
        if(req.body.fat  != null){updates['fat'] = req.body.fat;};
        if(req.body.carbs != null){updates['carbs'] = req.body.carbs;};
        if(req.body.protein!= null){updates['protein'] = req.body.protein;};
        if(req.body.salt  != null){updates['salt'] = req.body.salt;};
        if(req.body.img != null){updates['img'] = req.body.img;};

        if (!errors.isEmpty()) {
           res.json(errors) 
        }
        else {
            Ingredient.findOneAndUpdate
            var query = { name:  req.params.name};
            var retOptions = {_id:0, _v:0}
            Ingredient.findOneAndUpdate(query, updates, retOptions).exec(
                function (err,theingredient) {
                    if (err) {
                        return next(err); 
                    }
                    else{
                        res.json(theingredient);
                    }
                    }

            );
        }
    }
];