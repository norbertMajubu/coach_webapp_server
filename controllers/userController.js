var Mealplan = require('../models/mealplan')
var models = require('../models/user');
User  = models.User;
Role = models.Role;

var trainer_role_id = 0;
var client_role_id = 0;

Permission = models.Permission;

var async = require('async');

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');


exports.create_user_roles = function(){

    var permission_clients = new Permission(
        {name: 'have_clients'}
    )

    var permission_edit_db = new Permission(
        {name: 'edit_db'}
    )

    var permission_trained = new Permission(
        {name: 'have_trainer'}
    )

    Permission.findOne({ 'name': permission_clients.name }).exec( function(err, found_permission) {
        if (found_permission) {return;}
        else {permission_clients.save(function (err){});}});
    Permission.findOne({ 'name': permission_edit_db.name }).exec( function(err, found_permission) {
        if (found_permission) {return;}
        else {permission_edit_db.save(function (err){});}});
    Permission.findOne({ 'name': permission_trained.name }).exec( function(err, found_permission) {
        if (found_permission) {return;}
        else {permission_trained.save(function (err){});}});

    //permission_clients.save(function(err){});
    //permission_edit_db.save(function(err){});
    //permission_trained.save(function(err){});


    var role_trainer = new Role(
        {
            name: 'trainer',
            permissions: [permission_clients._id, permission_edit_db._id],
        }
    );
    var role_client = new Role(
        {
            name: 'client',
            permissions: [permission_trained._id],
        }
    )
    /*role_trainer.save(function (err) {
        if (err) { console.log("hiba a role mentese kozben" + err); res.json(err); }       
      });*/

    /*role_client.save(function (err) {
        if (err) { console.log("hiba a role mentese kozben" + err); res.json(err); }       
      });*/

    Role.findOne({ 'name': role_trainer.name }).exec( function(err, found_role) {
        if (found_role) {
            trainer_role_id   = found_role._id;
        
            return;
        }
        else {
            role_trainer.save(function (err){});
            trainer_role_id  = role_trainer._id;
        }});

    Role.findOne({ 'name': role_client.name }).exec( function(err, found_role) {
        if (found_role) {
            client_role_id = found_role._id;
            return;
        }
        else {
            role_client.save(function (err){});
            client_role_id = role_client._id;
        }});
    
    //client_role_id  = role_client._id;
    //trainer_role_id = role_trainer._id;
    
}


// Display list of all User.
exports.users_list = function(req,res,next){

    User.find()
    .sort([['name', 'ascending']])
    .exec(function (err, list_users) {
      if (err) { return next(err); }
      // Successful, so render.
      res.json(list_users);
     // res.render('user_list', { title: 'User List', list_users:  list_users});
    });
}

// Display detail page for a specific User.
exports.user_detail = function(req, res, next) {
    //TODO: JUST LIKE INGREDIETNS
    console.log("user details, belep")
    async.parallel({
        user: function(callback) {
            var query = { username:  req.params.username};
            var retOptions = {_id:0, __v:0}
            User.find(query, retOptions)
              .exec(callback);
        },
    }, function(err, results) {
        if (err) { return next(err); }
        if (results.user==null) { // No results.
            res.json('')
        }
        else{
            console.log(results.user[0])
            res.json(results.user[0])
        }
        
    });

};


// Display detail page for a specific User.
exports.user_search = function(req, res, next) {
    //TODO: JUST LIKE INGREDIETNS
    async.parallel({
        user: function(callback) {
            var query = { username:  new RegExp(".*" + req.params.username + ".*", "g")};
            var retOptions = {_id:0, _v:0}
            User.find(query, retOptions)
              .exec(callback);
        },
    }, function(err, results) {
        if (err) { return next(err); }
        if (results.user==null) { // No results.
            res.json('')
        }
        else{
            res.json(results.user)
        }
        
    });

};


// Handle User create on POST.
exports.user_create_post = [

    // Validate that the name field is not empty.
    body('username', 'User name required').isLength({ min: 1 }).trim(),

    // Sanitize (trim and escape) the name field.
    sanitizeBody('username').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);
    
        // Create a user object with escaped and trimmed data.

        var user_mealplan = new Mealplan({
            energy: 0,
            fat: 0,
            carbs: 0,
            protein: 0,
            recipes: []
        })
        user_mealplan.save();

        var user = new User(
            { 
              username: req.body.username,
              firstname: req.body.firstname,
              surname: req.body.surname,
              email: req.body.email,
  
              mealplan: user_mealplan._id,
              water: 0,
              steps:  0,
              trainer_id: '',
          
              clients: [],
          
              role : 0,
             }
          );   

     

        //TODO: TITKOSITANI HERE!!
        //adding a client:
        if(req.body.role == 1 || req.body.role == undefined){
            user.role = client_role_id;
          
        }
        else if(req.body.role == 0){
           
            user.role = trainer_role_id;
        }
   

        if (!errors.isEmpty()) {
            // There are errors. Render the form again with sanitized values/error messages.
          
            res.json('Empty input');
            //res.render('user_form', { title: 'Create User', user: user, errors: errors.array()});
        return;
        }
        else {
            // Data from form is valid.
            // Check if User with same name already exists.
            User.findOne({ 'username': req.body.username })
                .exec( function(err, found_user) {
                 
                     if (err) { return next(err); }

                     if (found_user) {
                         // User exists, redirect to its detail page.
                         res.redirect(found_user.url);
                     }
                     else {
                      
                         user.save(function (err) {
                           if (err) { res.json(err); }
                           else{
                          
                            // User saved. Redirect to user detail page.
                            res.json(user);     
                           }
                          
                         });

                     }

                 });
        }
    }
];

// Handle User delete on POST.
exports.user_delete_post = function(req, res, next) {

    async.parallel({
        user: function(callback) {
            User.findById(req.params.name).exec(callback);
        },
        user_mealplans: function(callback) {
            User.find({ 'user': req.params.name }).exec(callback);
        },
    }, function(err, results) {
        if (err) { return next(err); }
        // Success
        if (results.user_mealplans.length > 0) {
            // User has mealplans. Render in same way as for GET route.
            res.end('User belongs to existing mealplan') //res.render('user_delete', { title: 'Delete User', user: results.user, user_mealplans: results.user_mealplans } );
        }
        else {
            // User has no mealplans. Delete object and redirect to the list of users.
            User.findByIdAndRemove(req.body._id, function deleteUser(err) {
                if (err) { return next(err); }
                // Success - go to users list.
                else{res.end('');}
            });

        }
    });

};

// Handle User update on POST.
exports.user_update_post = [
   
    // Validate that the name field is not empty.
    body('username', 'User name required').isLength({ min: 1 }).trim(),
    
    // Sanitize (trim and escape) the name field.
    sanitizeBody('username').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {
       
        // Extract the validation errors from a request .
        const errors = validationResult(req);

    // Create a user object with escaped and trimmed data (and the old _id!)
        var updates = {};
        if(req.body.username != null){updates['username'] = req.body.username;};
        if(req.body.firstname!= null){updates['firstname'] = req.body.firstname;};
        if(req.body.surname  != null){updates['surname'] = req.body.surname;};
        if(req.body.email != null){updates['email'] = req.body.email;};

        if (!errors.isEmpty()) {
            // There are errors. Render the form again with sanitized values and error messages.
      
           res.json(errors) //res.render('user_form', { title: 'Update User', user: user, errors: errors.array()});
            
            //return;
        }
        else {
            User.findByIdAndUpdate(req.params.name, updates, {}, function (err,theuser) {
                if (err) { return next(err); }
                else{
                   
                    // Successful - redirect to user detail page.
                    res.json(theuser) //res.redirect(theuser.url);
                }
                  
                });
        }
    }
];