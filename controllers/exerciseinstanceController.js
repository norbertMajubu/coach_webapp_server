var ExerciseInstance = require('../models/exerciseinstance')
var Exercise = require('../models/exercise')
var async = require('async')

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

// Display list of all ExerciseInstances.
exports.exerciseinstance_list = function(req, res, next) {

  ExerciseInstance.find()
    .populate('exercise')
    .exec(function (err, list_exerciseinstances) {
      if (err) { return next(err); }
      // Successful, so render.
      res.render('exerciseinstance_list', { title: 'Exercise Instance List', exerciseinstance_list:  list_exerciseinstances});
    })

};

// Display detail page for a specific ExerciseInstance.
exports.exerciseinstance_detail = function(req, res, next) {

    ExerciseInstance.findById(req.params.id)
    .populate('exercise')
    .exec(function (err, exerciseinstance) {
      if (err) { return next(err); }
      if (exerciseinstance==null) { // No results.
          var err = new Error('Exercise copy not found');
          err.status = 404;
          return next(err);
        }
      // Successful, so render.
      res.render('exerciseinstance_detail', { title: 'Exercise:', exerciseinstance:  exerciseinstance});
    })

};

// Display ExerciseInstance create form on GET.
exports.exerciseinstance_create_get = function(req, res, next) {

     Exercise.find({},'title')
    .exec(function (err, exercises) {
      if (err) { return next(err); }
      // Successful, so render.
      res.render('exerciseinstance_form', {title: 'Create ExerciseInstance', exercise_list:exercises } );
    });

};

// Handle ExerciseInstance create on POST.
exports.exerciseinstance_create_post = [

    // Validate fields.
    body('exercise', 'Exercise must be specified').isLength({ min: 1 }).trim(),
    body('imprint', 'Imprint must be specified').isLength({ min: 1 }).trim(),
    body('due_back', 'Invalid date').optional({ checkFalsy: true }).isISO8601(),
    
    // Sanitize fields.
    sanitizeBody('exercise').trim().escape(),
    sanitizeBody('imprint').trim().escape(),
    sanitizeBody('status').trim().escape(),
    sanitizeBody('due_back').toDate(),
    
    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create a ExerciseInstance object with escaped and trimmed data.
        var exerciseinstance = new ExerciseInstance(
          { exercise: req.body.exercise,
            imprint: req.body.imprint,
            status: req.body.status,
            due_back: req.body.due_back
           });

        if (!errors.isEmpty()) {
            // There are errors. Render form again with sanitized values and error messages.
            Exercise.find({},'title')
                .exec(function (err, exercises) {
                    if (err) { return next(err); }
                    // Successful, so render.
                    res.render('exerciseinstance_form', { title: 'Create ExerciseInstance', exercise_list : exercises, selected_exercise : exerciseinstance.exercise._id , errors: errors.array(), exerciseinstance:exerciseinstance });
            });
            return;
        }
        else {
            // Data from form is valid
            exerciseinstance.save(function (err) {
                if (err) { return next(err); }
                   // Successful - redirect to new record.
                   res.redirect(exerciseinstance.url);
                });
        }
    }
];



// Display ExerciseInstance delete form on GET.
exports.exerciseinstance_delete_get = function(req, res, next) {

    ExerciseInstance.findById(req.params.id)
    .populate('exercise')
    .exec(function (err, exerciseinstance) {
        if (err) { return next(err); }
        if (exerciseinstance==null) { // No results.
            res.redirect('/exerciseinstances');
        }
        // Successful, so render.
        res.render('exerciseinstance_delete', { title: 'Delete ExerciseInstance', exerciseinstance:  exerciseinstance});
    })

};

// Handle ExerciseInstance delete on POST.
exports.exerciseinstance_delete_post = function(req, res, next) {
    
    // Assume valid ExerciseInstance id in field.
    ExerciseInstance.findByIdAndRemove(req.body.id, function deleteExerciseInstance(err) {
        if (err) { return next(err); }
        // Success, so redirect to list of ExerciseInstance items.
        res.redirect('/exerciseinstances');
        });

};

// Display ExerciseInstance update form on GET.
exports.exerciseinstance_update_get = function(req, res, next) {

    // Get exercise, authors and genres for form.
    async.parallel({
        exerciseinstance: function(callback) {
            ExerciseInstance.findById(req.params.id).populate('exercise').exec(callback)
        },
        exercises: function(callback) {
            Exercise.find(callback)
        },

        }, function(err, results) {
            if (err) { return next(err); }
            if (results.exerciseinstance==null) { // No results.
                var err = new Error('Exercise copy not found');
                err.status = 404;
                return next(err);
            }
            // Success.
            res.render('exerciseinstance_form', { title: 'Update  ExerciseInstance', exercise_list : results.exercises, selected_exercise : results.exerciseinstance.exercise._id, exerciseinstance:results.exerciseinstance });
        });

};

// Handle ExerciseInstance update on POST.
exports.exerciseinstance_update_post = [

    // Validate fields.
    body('exercise', 'Exercise must be specified').isLength({ min: 1 }).trim(),
    body('imprint', 'Imprint must be specified').isLength({ min: 1 }).trim(),
    body('due_back', 'Invalid date').optional({ checkFalsy: true }).isISO8601(),
    
    // Sanitize fields.
    sanitizeBody('exercise').trim().escape(),
    sanitizeBody('imprint').trim().escape(),
    sanitizeBody('status').trim().escape(),
    sanitizeBody('due_back').toDate(),
    
    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create a ExerciseInstance object with escaped/trimmed data and current id.
        var exerciseinstance = new ExerciseInstance(
          { exercise: req.body.exercise,
            imprint: req.body.imprint,
            status: req.body.status,
            due_back: req.body.due_back,
            _id: req.params.id
           });

        if (!errors.isEmpty()) {
            // There are errors so render the form again, passing sanitized values and errors.
            Exercise.find({},'title')
                .exec(function (err, exercises) {
                    if (err) { return next(err); }
                    // Successful, so render.
                    res.render('exerciseinstance_form', { title: 'Update ExerciseInstance', exercise_list : exercises, selected_exercise : exerciseinstance.exercise._id , errors: errors.array(), exerciseinstance:exerciseinstance });
            });
            return;
        }
        else {
            // Data from form is valid.
            ExerciseInstance.findByIdAndUpdate(req.params.id, exerciseinstance, {}, function (err,theexerciseinstance) {
                if (err) { return next(err); }
                   // Successful - redirect to detail page.
                   res.redirect(theexerciseinstance.url);
                });
        }
    }
];
